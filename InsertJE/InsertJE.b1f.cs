﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace InsertJE
{
    [FormAttribute("InsertJE.InsertJE", "InsertJE.b1f")]
    class InsertJE : UserFormBase
    {
        public InsertJE()
        {
        }

        private SAPbouiCOM.Button btnInsert;
        public override void OnInitializeComponent()
        {
            this.btnInsert = ((SAPbouiCOM.Button)(this.GetItem("btnInsert").Specific));
            this.btnInsert.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.btnInsert_ClickBefore);
            this.OnCustomInitialize();

        }

        public override void OnInitializeFormEvents()
        {
        }

        
        private void OnCustomInitialize()
        {

        }

        private void btnInsert_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            SAPbobsCOM.Company oCompany = (SAPbobsCOM.Company)Application.SBO_Application.Company.GetDICompany();
            SAPbobsCOM.JournalEntries oJE = (SAPbobsCOM.JournalEntries)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries);

            //JE
            oJE.Series = 17;
            oJE.ReferenceDate = DateTime.UtcNow;
            oJE.TaxDate = DateTime.UtcNow;
            oJE.DueDate = DateTime.UtcNow;
            
            //Line 1
            oJE.Lines.ShortName = "CC004462";                //use ShortName for BP code
            oJE.Lines.ContraAccount = "00400000";
            oJE.Lines.ControlAccount = "13110100";
            oJE.Lines.Debit = 1000;
            oJE.Lines.ProjectCode = "CS";
            oJE.Lines.LineMemo = "test insert JE";
            oJE.Lines.DueDate = DateTime.UtcNow;
            oJE.Lines.TaxDate = DateTime.UtcNow;
            oJE.Lines.DueDate = DateTime.UtcNow;
            oJE.Lines.Add();

            //Line 2
            oJE.Lines.AccountCode = "00400000";          // use Account code for account code
            oJE.Lines.ContraAccount = "CC004462";
            oJE.Lines.ControlAccount = "00400000";
            oJE.Lines.Credit = 1000;
            oJE.Lines.ProjectCode = "CS";
            oJE.Lines.LineMemo = "";
            oJE.Lines.DueDate = DateTime.UtcNow;
            oJE.Lines.TaxDate = DateTime.UtcNow;
            oJE.Lines.DueDate = DateTime.UtcNow;
            oJE.Lines.Add();

            int result = oJE.Add();
            if(result != 0){
                Application.SBO_Application.SetStatusBarMessage(oCompany.GetLastErrorDescription(), SAPbouiCOM.BoMessageTime.bmt_Medium, true);
            }
            else
            {
                Application.SBO_Application.MessageBox("Success");
            }
        }
    }
}
